

import numpy as np
from guietta import Gui, PG, ___, III, _, VS

gui = Gui(
  [  PG('plot'),  ___, ___, VS('slider') ],
  [     III     , III, III,     III      ],
  [     III     , III, III,     III      ],
  [     III     , III, III,  '^^^ Move the slider'  ],
 )

gui.run()